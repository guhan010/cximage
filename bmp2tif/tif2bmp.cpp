
#include "tif2bmp.h"
#include <cassert>
#include <io.h>
#include <direct.h>

static CString getCurrentDirectory(){
	TCHAR path[1024] = {0};
	DWORD err  = GetCurrentDirectory(sizeof(path), path);
	
	return CString(path);
};

static CString getFileName(CString fileName){
	int c = fileName.GetLength() - fileName.ReverseFind(_T('\\')) - 1;
	CString ret = fileName.Right(c);
	ret  = ret.Left(ret.Find(_T(".")));
	return ret;
};


shared_ptr<CTif2Bmp> CTif2Bmp::_self;
CTif2Bmp::CTif2Bmp(){
	if(_access(".\\cache", 0) != 0)
	{
		_mkdir(".\\cache");
	}
}

CTif2Bmp::~CTif2Bmp(){
}


CTif2Bmp* CTif2Bmp::instance(){
	if(!_self){
		_self.reset(new CTif2Bmp);
	}
	return _self.get();
}

void CTif2Bmp::setTifFile(CString szFile){
	m_szFileName = szFile;
}

CString CTif2Bmp::getTifFile() const{
	return m_szFileName;
}

list<CString> CTif2Bmp::deal(){
	list<CString> ret;
	if(m_szFileName.IsEmpty()){
		assert(0);
		return ret;
	}

	CxImage tif;
	tif.Load(m_szFileName, CXIMAGE_FORMAT_TIF);
	unsigned int count = tif.GetNumFrames();
	if(count == 0 ){
		assert(0);
		return ret;
	}

	unsigned int i = 0;
	for(; i < count; i++){
		tif.SetFrame(i);
		
		//load new frame from tiff
		tif.Load(m_szFileName, CXIMAGE_FORMAT_TIF);

		CxImage image;
		image.Copy(tif);

		CString dir = getCurrentDirectory();
		CString fileName;
		fileName.Format(_T("%s\\cache\\%s%03d.bmp"), dir, getFileName(m_szFileName), i);
		
		if(image.Save(fileName, CXIMAGE_FORMAT_BMP)){
			ret.push_back(fileName);
		}else{
			assert(0);
		}
	}
	
	return ret;
}