/*
@
@老版本的白板xml解析类，该版本的xml样式如下:
<SharePages wbstatus="0" current="4" lastidx="80">
	<SharePage pageid="{0981FD07-093A-4FAA-B708-F58B4D555CF2}" pageidx="0" isweb="0" pagetype="4" url="http://www.tt-xx.com/UploadDataDir/2011-03-13/{AEDDCCD6-008B-428B-BD4F-22C39AAC43F6}.zip" cur="0" top="31" left="0" ptx="0" pty="0">
		<IMK>
			<LN sy='0' cl='255' sz='2' xs='11,276,' ys='670,670,'/>
			<LN sy='0' cl='255' sz='2' xs='323,322,321,320,320,320,320,320,320,320,320,320,320,320,' ys='689,689,692,693,694,695,696,697,698,699,699,700,701,701,'/>
			<REC X1='319' Y1='742' X2='413' Y2='761' sy='0' cl='255' sz='2' sl='0'/>
			</IMK>
	</SharePage>
</SharePages>
@
*/

#ifndef _DEALWHITEBROAD_H_
#define _DEALWHITEBROAD_H_
#include <afx.h>
//使用msxml3，vs2010以后版本升级为msxml6，所以使用时要引入该库
#import <msxml3.dll>
#include <list>

//xml元素基类
class CXmlItem{
public:
	CXmlItem();
	CXmlItem(CString& szName);
	virtual~CXmlItem();

	//xml 处理
public:
	virtual bool analyzNode(MSXML2::IXMLDOMNodePtr pNode) = 0;
	//
public:
	CString name();
	void name(CString& n);
private:
	CString m_szName;
};
class CDrawItem: public CXmlItem{
public:
	typedef struct{
		CString sy;
		CString cl;
		CString sz;
		CString xs;
		CString ys;
	}ATT;

private:

};

//do nothing
class CIMK:public CXmlItem{
public:
	bool analyzNode(MSXML2::IXMLDOMNodePtr pNode);

private:
	std::list<CDrawItem*> m_ltDramItem;
};

class CPageItem: public CXmlItem{
public:
	typedef struct {
		CString pageid;
		CString pageidx;
		CString isweb;
		CString pagetype;
		CString url;
		CString cur;
		CString top;
		CString left;
		CString ptx;
		CString pty;
	}ATT;
	CPageItem();
	~CPageItem();
public:
	bool analyzNode(MSXML2::IXMLDOMNodePtr pNode);

	CPageItem::ATT getAtt() const;
private:
	std::list<CXmlItem*> m_ltIMK;
	CPageItem::ATT m_att;
};



class CPageContainer{
public:
	static bool analyzPage(CString path);


};


#endif //<_DEALWHITEBROAD_H_