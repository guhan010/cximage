

/*
@
@tiff to bmp
@using cximage lib
@
*/

#ifndef _TIF2_BMP_H_
#define _TIF2_BMP_H_
#include <afx.h>
#include "ximage.h"

#include <list>
#include <memory>


using namespace std;
class CTif2Bmp{
public:
	static CTif2Bmp* instance();

	void setTifFile(CString szFile);
	CString getTifFile() const;

	list<CString> deal();

public:
	~CTif2Bmp();
private:
	CTif2Bmp();
	
private:
	static shared_ptr<CTif2Bmp> _self;

private:
	CString m_szFileName;
};


#endif //<_TIF2_BMP_H_